#!/usr/bin/env python
#-*-encoding: utf-8 -*-
from lxml import etree
import datetime
import cookielib
import sqlite3
import urllib2
import urllib
import optparse
import pprint
import json

class Feed (object):
    def __init__ (self, cate, bid, title, url, section, opener):
        self.cate = cate
        self.bid = bid
        self.title = title
        self.url = url
        self.section = section
        self.opener = opener

    def __repr__ (self):
        return '[%s, %s, %s]' % (self.title.decode ('utf-8'), self.bid, self.cate)

    def __call__ (self):
        print 'fetch====================', self.url
        fetch_board ('http://m.newsmth.net' + self.url, self.section, self.opener)

    def printself (self):
        print '[%s, %s, %s, %s]' % (self.title, self.bid, self.cate, self.section)

all_boards = {}

def fetch_hot (url):
    res = urllib2.urlopen (url)
    parser = etree.HTMLParser (encoding='utf-8')
    tree = etree.parse (res, parser = parser)
    r = tree.xpath ("//ul[@class='slist sec']//li//a")
    for x in r:
        print x.text
        href = x.attrib['href']
        try:
            bid = href.split ('/')[-2]
            print '[%s]' % all_boards[bid].title
        except:
            pass

def fetch_board(url, section, opener):
    if not section:
        print 'fetch %s url error!!, no section' % url
    res = opener.open (url)
    parser = etree.HTMLParser (encoding='utf-8')
    tree = etree.parse (res, parser = parser)
    r = tree.xpath ('//li/a[@href]')
    for x in r:
        try:
            url = x.xpath('./@href')[0]
            bid = url.split('/')[-1]
            cate = url.split('/')[-2]
        except:
            bid = None
            url = None
            print 'exception on parse url'

        if cate != 'board' and cate != 'section':
            print 'error, cate: %s !!!!!! for url : %s in ection: %s' %(cate,  url,  section)
        print 'find li: %s cate: %s' % (x.text, cate)
        title = x.xpath ('./text()')[0]

        l = Feed (cate, bid, title, url, section, opener)
        all_boards[bid] = l
        if cate == 'section':
            print 'fetching....... section %s' % title
            l ()

def login (username, password):
    cj = cookielib.CookieJar ()
    opener = urllib2.build_opener( urllib2.HTTPCookieProcessor (cj))
    login_url = 'http://m.newsmth.net/user/login'
    params = {
            'id' : username,
            'passwd' : password,
            'save' : 'on'
            }
    urllib2.install_opener( opener )
    resp = opener.open (login_url, urllib.urlencode(params))
    resp.read ()

    return opener

def ajax_session (opener):
    request = urllib2.Request ('http://www.newsmth.net/nForum/user/ajax_session.json')
    request.add_header('X-Requested-With', 'XMLHttpRequest')

    resp = opener.open (request)
    a = resp.read ()
    print a

def attach (opener):
    request = urllib2.Request ('http://www.newsmth.net/nForum/att/Apple/ajax_add.json?name=3839205_120343654312_2.jpg')
    request.add_header('X-Requested-With', 'XMLHttpRequest')
    request.add_data (open('1.jpg', 'r').read())
    resp = opener.open (request)
    print request.data
    print request.header_items();
    a = resp.read ()
    print resp
    print a

    #request = urllib2.Request ('http://www.newsmth.net/nForum/att/Apple/ajax_add.json?name=3839205_120343654312_3.jpg')
    #request.add_header('X-Requested-With', 'XMLHttpRequest')
    #request.add_data ('afdafafdsafafdasfa')
    #resp = opener.open (request)
    #a = resp.read ()
    #print a

def post_delete (opener, file_id):
    request = urllib2.Request ('http://www.newsmth.net/nForum/article/Apple/ajax_delete/697911.json')

def post_edit (opener, file_id):
    request = urllib2.Request ('http://www.newsmth.net/nForum/article/Apple/ajax_edit/696293.json')
    params=  {
            'subject' : '',
            'content' : ''
            }

def post_new (opener):
    request = urllib2.Request ('http://www.newsmth.net/nForum/article/Test/ajax_post.json')
    params = {
            'content' : '',
            'id' : 0,
            'signature' : 0,
            'subject' : 'afdafa'
            }

def post_forward (opener, user):
    request = urllib2.Request ('http://www.newsmth.net/nForum/article/Test/ajax_forward/876568.json')
    params = {
            'target': 'userid'
            }
    request.add_header('X-Requested-With', 'XMLHttpRequest')
    resp = opener.open (request)
    a = resp.read ()
    print a

def favorite (opener):
    'http://www.newsmth.net/nForum/fav/0.json?_t=1345814291227'
    t = datetime.datetime.now () - datetime.datetime (1970,1,1)
    t = '%d' % (int(t.total_seconds()))
    
    request = urllib2.Request ('http://www.newsmth.net/nForum/fav/0.json?_t=%s' % t)
    request.add_header('X-Requested-With', 'XMLHttpRequest')
    resp = opener.open (request)
    a = resp.read ()
    favs = json.loads (a.decode ('gbk'))

    for x in favs:
        print x['type']
        pprint.pprint (x)

def mail_info ():
    'http://www.newsmth.net/nForum/mail/outbox/57.json'

def mail_post ():
    'http://www.newsmth.net/nForum/mail/inbox/ajax_send.json'
    params = {
            'backup' : 'on',
            'content' : 'aaaa',
            'id' : 'kerberos',
            'num' : 82,
            'signature' : 0,
            'title' : 'Title'
            }

def friends_list (opener):
    request = urllib2.Request ('http://www.newsmth.net/nForum/friend/ajax_list.json')
    request.add_header('X-Requested-With', 'XMLHttpRequest')
    resp = opener.open (request)
    a = resp.read ()
    print a
def friends_add ():
    'http://www.newsmth.net/nForum/friend/ajax_add.json?id=rosydusk'
    param_get = {
            'id' : 'friend_id'
            }
def user_query ():
    'http://www.newsmth.net/nForum/user/query/rosydusk.json'
    params = {
            }

def main ():
    usage = "%prog [options]"
    p = optparse.OptionParser (usage)
    p.add_option ('-p', '--password', action = 'store', dest = 'password', type = 'string')
    p.add_option ('-u', '--username', action = 'store', dest = 'username', type = 'string')

    options, args = p.parse_args ()

    if (not (options.username and options.password)):
        p.print_help ()
        p.error('error arguments "username and password"')

    opener = login (options.username, options.password)

#    ajax_session (opener)
#    attach (opener)
#    friends_list (opener)
#    favorite (opener)

    for x in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A']:
        fetch_board ('http://m.newsmth.net/section/%s' % x, x, opener)

if __name__ == '__main__':
    main ()
    con = sqlite3.connect ('UcSMTH.cache')
    con.execute ("create table if not exists board (id text primary key, name text, section text)")
    con.execute ("create table if not exists section (id text primary key, name text, section text)")

    board_count = 0
    print 'boards'
    for v in all_boards.values():
        if v.cate == 'board':
            board_count += 1
            con.execute ("insert or replace into board (id, name, section) values ('%s', '%s', '%s')" %
                    (v.bid, v.title, v.section))
    print 'add board:', board_count

    print 'sections'
    section_count = 0
    for v in all_boards.values():
        if v.cate == 'section':
            section_count += 1
            #con.execute ("insert or replace into section (id, name, section) values ('%s', '%s', '%s')" %
            #        (v.bid, v.title, v.section))

    print 'add section:', section_count

    print 'allboards:', len( all_boards)

    for x in zip (['topTen', 'recommend', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A'],
            ['十大热门话题', '推荐', '社区管理','国内院校', '休闲娱乐', '游戏天地', '体育健身', '社会信息', '知识感性', '文化人文', '学术科学', '电脑技术', '关闭版面']):
        con.execute ("insert or replace into section (id, name, section) values ('%s', '%s', '%s')" %
                (x[0], x[1], x[0]))
    con.commit ()
