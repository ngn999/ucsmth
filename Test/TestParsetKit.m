//
//  TestParsetKit.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "ParseKit.h"
#import "USArticleContentTokenizer.h"
#import "TestParsetKit.h"

////////////////////////////////////////////////////////////////////////////////
@interface Assembly1 : NSObject
@end

@implementation Assembly1
- (void) parser: (PKParser*) p didMatchBoardPageHref: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}

- (void) parser: (PKParser*) p didMatchBoardId: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}

- (void) parser: (PKParser*) p didMatchBoardPages: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}

- (void) parser: (PKParser*) p didMatchPageNumberRule: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}

- (void) parser: (PKParser*) p didMatchPageIndex: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}

- (void) parser: (PKParser*) p didMatchPageTotal: (PKAssembly*) a
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"%@", a);
}
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@interface Assembly2 : NSObject
@end
/*
 static NSString* g = @""
 @"@start = pageNumberRule;"
 @"pageNumberRule = ('下页' '|')? ('尾页' '|')? pageIndex '|' pageTotal '|' '转到' (Word | Number | Symbol)*;"
 @"indexPage = Number;"
 @"totalPage = Number;"
 @"";
*/
@implementation Assembly2
@end
////////////////////////////////////////////////////////////////////////////////

@implementation TestParsetKit
+ (void) testParseKitBoardPages
{
    PKTokenizer* t;
    t = [PKTokenizer tokenizerWithString: @"/board/SMTH.EDU?p=38"];
    [t.wordState setWordChars: YES from:'.' to:'.'];
    PKToken* eof = [PKToken EOFToken];
    PKToken *tok = nil;
    while ((tok = [t nextToken]) != eof) {
        UCBREW_LOG(@"%@, %@", tok, [tok debugDescription]);
    }
}

+ (void) testParseKitBoardPages2
{
    PKTokenizer* t;
    t = [PKTokenizer tokenizerWithString: @"/board/SMTH.EDU/1?p=38"];
    [t.wordState setWordChars: YES from:'.' to:'.'];
    PKToken* eof = [PKToken EOFToken];
    PKToken *tok = nil;
    while ((tok = [t nextToken]) != eof) {
        UCBREW_LOG(@"%@, %@", tok, [tok debugDescription]);
    }
}

+ (void) test3
{
    static NSString* g = @""
        @"@wordChars = '.';"
    @"@start = boardPageHref;"
        @"boardPageHref = '/' 'board' '/' boardId ('/' Number)? '?' 'p' '=' boardPages;"
        @"boardId = Word;"
        @"boardPages = Number;"
        @"";    
    static NSString* g2 = @""
            @"@start = pageNumberText;"
            @"pageNumberText = Word '|' Word '|' indexPage '|' totalPage '|' Word Any*;"
            @"indexPage = Number;"
            @"totalPage = Number;"
            @"";
    Assembly1* a = [[Assembly1 alloc] init];
    PKParser* p = [[PKParserFactory factory] parserFromGrammar:g assembler: a];
    [p parse: @"/board/测试/5?p=133"];
    [a release];
    PKReleaseSubparserTree(p);
    p = [[PKParserFactory factory] parserFromGrammar:g2 assembler:a];
    [p parse: @"First | Last | 1/2 | goto .."];
}

+ (void) test4
{
    static NSString* g = @""
            @"@start = pageNumberRule;"
            @"pageNumberRule = (Word '|') (Word '|') indexPage '|' totalPage '|' Word Any*;"
            @"indexPage = Number;"
            @"totalPage = Number;"
            @"";
    NSString* s = @"秒回3700确实很疑惑就12件装备平均每件250啊【 在 valkyrievv (小小v--努力刷声望中) 的大作中提到: 】: dps完全没法看，护甲全抗开着能量护甲的也没啥好看的: 秒回是亮点，但是我总觉得不现实: 有monk罩着的吧--FROM 60.247.49.*";
    PKTokenizer* t = nil;
    t = [USArticleContentTokenizer tokenizerWithString: s];
    //[t.wordState setWordChars: YES from:'.' to:'.'];
    PKToken* eof = [PKToken EOFToken];
    PKToken *tok = nil;
    while ((tok = [t nextToken]) != eof) {
        UCBREW_LOG(@"%@, %@", tok, [tok debugDescription]);
    }
#if 0
    Assembly2* b = [[Assembly2 alloc] init];
    UCBREW_LOG(@"%@", b);
    PKParser* p = [[PKParserFactory factory] parserFromGrammar: g assembler: b];
    UCBREW_LOG(@"%@", p);
    [p parse:  @"Next | Last | 1/11 | Go "];
    [p parse:  @"下页 | 尾页 | 1/11 | 转到  "];
    [p parse:  @"1/11|转到  "];
    PKReleaseSubparserTree( p);
#endif
}
@end
