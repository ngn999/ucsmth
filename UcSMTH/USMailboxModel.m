//
//  USMailboxModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "DDXML+HTML.h"
#import "ParseKit.h"
#import "RegexKitLite.h"
#import "USURLRequest.h"
#import "USURLHtmlResponse.h"
#import "USURLRequestDelegate.h"
#import "USMailFeed.h"
#import "USMailboxModel.h"

@interface USMailboxModel () <USURLRequestDelegate>
@property (nonatomic, copy, readwrite) NSString* boxId;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, assign) NSUInteger indexOfPage;
@property (nonatomic, assign) NSUInteger totalOfPage;
@property (nonatomic, retain, readwrite) NSMutableArray* mailFeeds;
@end

@implementation USMailboxModel
@synthesize indexOfPage;
@synthesize totalOfPage;
@synthesize startIndexOfPage;
@synthesize boxId;
@synthesize mailFeeds;

+ (id) modelWithBoxId:(NSString *) aBoxId startIndexOfPage:(NSUInteger)aStartIndexOfPage
{
    USMailboxModel* model = [[USMailboxModel alloc] initWithBoxId: aBoxId startIndexOfPage: aStartIndexOfPage];
    return [model autorelease];
}

- (id) initWithBoxId: (NSString*) aBoxId startIndexOfPage: (NSUInteger) aStartIndexOfPage
{
    self = [super init];
    if (self) {
        self.boxId = aBoxId;
        self.startIndexOfPage = aStartIndexOfPage;
        self.totalOfPage = 0;
        self.indexOfPage = aStartIndexOfPage;
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(mailFeeds);
    UCBREW_RELEASE(boxId);
    [super dealloc];
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (!self.isLoading
        && UcBrew_IsNotEmptyString(self.boxId)) {
        if (more) {
            self.indexOfPage ++;
        } else {
            self.indexOfPage = self.startIndexOfPage;
            NSMutableArray* a = [[NSMutableArray alloc] initWithCapacity: 20];
            self.mailFeeds = a;
            [a release];
        }
        NSString* url = [NSString stringWithFormat: @"http://m.newsmth.net/mail/%@?p=%d", self.boxId, self.indexOfPage];
        USURLRequest* req = [USURLRequest requestWithURL: url delegate: self];
        req.cachePolicy = TTURLRequestCachePolicyNetwork;
        req.response = [USURLHtmlResponse response];
        [req send];
    }
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse* resp = UCBREW_DYNAMIC_CAST(request.response,  USURLHtmlResponse);
    if (resp) {
        [self processHtmlDocument: resp.htmlDocument];
    }
    [super requestDidFinishLoad: request];
}

- (void) processHtmlDocument: (DDXMLDocument*) htmlDocument
{
    NSString* xpath = nil;
    NSArray* nodes = nil;
    xpath = @"//div[@id='m_main']/div[@class='sec nav'][2]/form";
    nodes = [htmlDocument nodesForXPath: xpath error: nil];
    @try {
        NSString* page_string = [[nodes objectAtIndex: 0] stringValue];
        PKTokenizer* t = [PKTokenizer tokenizerWithString: page_string];
        PKToken* eof = [PKToken EOFToken];
        PKToken* tok;
        NSUInteger num_count = 0;
        while ( (tok = [t nextToken]) != eof) {
            if (tok.isNumber) {
                num_count ++;
                if (num_count == 2) {
                    UCBREW_LOG_OBJECT(page_string);
                    UCBREW_LOG(@"%f", tok.floatValue);
                    self.totalOfPage = (NSUInteger) floor(tok.floatValue);
                    UCBREW_LOG(@"%d", self.totalOfPage);
                }
            }
        }
    }
    @catch (NSException *exception) {
    }
    xpath = @"//div[@id='m_main']/ul[@class='list sec']/li[a[@href]]";
    nodes = [htmlDocument nodesForXPath: xpath error: nil];
    for (DDXMLElement* e in nodes) {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        NSString* mailSubject = nil;
        NSString* mailAuthorId = nil;
        NSString* mailId = nil;
        NSString* mailDate = nil;
        NSString* mailHref = nil;
        NSString* mailAuthorHref = nil;
        DDXMLNode* node = nil;
        @try {
            mailHref = [[[e nodesForXPath: @"./a[1]/@href" error:nil] objectAtIndex: 0] stringValue];
            static NSString* regexMailId = @"\\/mail\\/(inbox|outbox|deleted)\\/(\\d+)";
            mailId = [mailHref stringByMatching: regexMailId capture: 2L];
            
            mailSubject = [[[e nodesForXPath: @"./a[1]" error:nil] objectAtIndex: 0] stringValue];
            mailAuthorId = [[[e nodesForXPath: @"./a[2]" error:nil] objectAtIndex: 0] stringValue];
            
            
            mailDate = [[e childAtIndex: 4] stringValue];
            if ([[mailDate substringWithRange: NSMakeRange(0, 1)] isEqualToString: @"|"]) {
                mailDate = [mailDate stringByReplacingCharactersInRange: NSMakeRange(0, 1) withString:@""];
            }
            
            UCBREW_LOG(@"=================");
            UCBREW_LOG_OBJECT(mailId);
            UCBREW_LOG_OBJECT(mailHref);
            UCBREW_LOG_OBJECT(mailAuthorId);
            UCBREW_LOG_OBJECT(mailSubject);
            UCBREW_LOG_OBJECT(mailDate);
            UCBREW_LOG(@"=================");
            if (UcBrew_IsNotEmptyString(mailId)
                && UcBrew_IsNotEmptyString(mailAuthorId)
                && UcBrew_IsNotEmptyString(mailSubject)
                && UcBrew_IsNotEmptyString(mailDate)) {
                USMailFeed* feed = [[[USMailFeed alloc] init] autorelease];
                feed.mailId = mailId;
                feed.mailAuthor = mailAuthorId;
                feed.mailSubject = mailSubject;
                feed.mailDate = mailDate;
                [self.mailFeeds addObject: feed];
            }
        }
        @catch (NSException *exception) {
        
        }
        [pool release];
    }
}
@end