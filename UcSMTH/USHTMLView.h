//
//  USHTMLView.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcHTMLView.h"
#import "CMHTMLView.h"

@interface USHTMLView : UIWebView
@property (nonatomic, copy) NSString* fontFamily;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) CGFloat maxPortraitWidth;
@property (nonatomic, assign) CGFloat maxLandscapeWidth;
@property (nonatomic, copy) NSString* addtionalStyle;
@property (nonatomic, readonly) UIScrollView* scrollView;

- (void) loadHTMLBody: (NSString*) htmlBody baseUrl: (NSString*) baseUrl;
@end
