//
//  USTableGrayTextItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableGrayTextItemCell.h"
#import "USTableGrayTextItem.h"

@implementation USTableGrayTextItem

- (Class) cellClass
{
    return [USTableGrayTextItemCell class];
}
@end
