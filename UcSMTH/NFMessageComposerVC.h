//
// Created by kerberos on 12-8-24.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define kNFMessageComposerAttachmentName @"filename"
#define kNFMessageComposerAttachmentData @"data"
#define kNFMessageComposerAttachmentMimeType @"mimetype"
#define kNFMessageComposerAttachemntSize @"size"
#define kNFMessageComposerAttachementAsset @"asset"

typedef enum  {
    NFMessageComposerHeaderFieldNone = 0x00,
    NFMessageComposerHeaderFieldToMask = 0x01,
    NFMessageComposerHeaderFieldSubjectMask = 0x02,
    NFMessageComposerHeaderFieldAttachmentMask = 0x04,
} NFMessageComposerHeaderFieldMask;

@class NFMessageComposerVC;

@protocol NFMessageComposerVCDelegate <NSObject>
@optional
- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC;
- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC;
@end

@interface NFMessageComposerVC : UIViewController
@property (nonatomic, assign) NFMessageComposerHeaderFieldMask headerFieldMask;
@property (nonatomic, copy) NSString* to;
@property (nonatomic, copy) NSString* subject;
@property (nonatomic, copy) NSString* body;
@property (nonatomic, readonly) NSArray* attachments;
@property (nonatomic, assign) NSUInteger maxOfAttachments;

@property (nonatomic, copy) NSString* titleForSending;
@property (nonatomic, copy) NSString* titleForSuccess;
@property (nonatomic, copy) NSString* ttileForError;

@property (nonatomic, assign) id<NFMessageComposerVCDelegate> delegate;

- (id) initWithBoardId: (NSString*) boardId articleId: (NSString*) articleId to: (NSString*) to
               subject: (NSString*) subject body: (NSString*) body
              delegate: (id <NFMessageComposerVCDelegate>) delegate;

+ (id) viewControllerWithBoardId: (NSString*) boardId articleId: (NSString*) articleId to: (NSString*) to
                         subject: (NSString*) subject body: (NSString*) body
                        delegate: (id <NFMessageComposerVCDelegate>) delegate;

+ (id) viewControllerWithFieldMask: (NFMessageComposerHeaderFieldMask) mask;

- (id) initWithFiledMask: (NFMessageComposerHeaderFieldMask) mask;

- (void) startSend;
- (void) didFinishSend;
- (void) didFailWithError: (NSError*) error;
@end