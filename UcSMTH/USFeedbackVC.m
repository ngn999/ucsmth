//
//  USFeedbackVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableTextViewItem.h"
#import "USTableTextViewCell.h"
#import "USFeedbackVC.h"

@interface USFeedbackVC ()
@property (nonatomic, retain) UITextView* textView;
@end

@implementation USFeedbackVC
@synthesize textView;

+ (id) viewController
{
    USFeedbackVC* vc = [[USFeedbackVC alloc] initWithStyle: UITableViewStyleGrouped];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    self.textView = [[[UITextView alloc] initWithFrame: CGRectZero] autorelease];
    self.textView.font = [UIFont systemFontOfSize: 16.0f];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    MwfTableData* table_data = [MwfTableData createTableData];
    [table_data addRow: self.textView];
    self.tableData = table_data;
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                                            target: self
                                                                                            action: @selector(handleSendAction)]
                                              autorelease];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.textView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) handleSendAction
{
    
}

- (NSString*) title
{
    return NSLocalizedString (@"Feedback", nil);
}

- (void) dealloc
{
    UCBREW_RELEASE (textView);
    [super dealloc];
}


- (MwfTableData*) createAndInitTableData
{
    return nil;
}

- $cellFor (UITextView)
{
    NSString* cell_identifier = [NSStringFromClass([UIViewController class]) stringByAppendingString:@"Cell"];
    USTableTextViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier: cell_identifier];
    if (cell == nil) {
        cell = [[USTableTextViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: cell_identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- $configCell (UITextView, USTableTextViewCell)
{
    cell.textView = item;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [[self tableDataForTableView: tableView] objectForRowAtIndexPath: indexPath];
    if ([object isKindOfClass: [UITextView class]]) {
        return 160.0f;
    }
    return 0.0f;
}

@end