//
//  USFavorVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableViewController.h"

@interface USFavorVC : USTableViewController
@property (nonatomic, copy) NSString* dirId;
@property (nonatomic, copy) NSString* dirName;
+(id) viewController;
+(id) viewControllerWithDirName: (NSString*) dirName dirId: (NSString*) dirId;
@end
