//
//  USMailLVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USMailLVC.h"
#import "USMailboxVC.h"
#import "USNavigationController.h"

@interface USMailLVC ()

@end

@implementation USMailLVC
+ (id) viewController
{
    USMailLVC* vc = [[USMailLVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) handleComposeAction
{
#if 0
    USMailComposerVC* vc = [USMailComposerVC viewControllerWithMailId: nil
                                                               userId: nil
                                                              subject: nil
                                                                boxId: nil
                                                             delegate: self];
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
#endif
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	/* disable mail composer in mailbox list.
	 * send mail with ajax need refresh session. but in this flow, no session update, so send mail always error.
	 */
#if 0
    UIBarButtonItem* compose_item = nil;
    compose_item = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCompose
                                                                  target: self
                                                                  action: @selector(handleComposeAction)]
            autorelease];
    self.navigationItem.rightBarButtonItem = compose_item;
#endif
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    return NSLocalizedString(@"Mail", nil);
}

- (void) createModel
{
    self.dataSource = [TTListDataSource dataSourceWithObjects:
                       [TTTableTextItem itemWithText: NSLocalizedString(@"Inbox", nil)
                                           accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText: NSLocalizedString(@"Sent", nil)
                                           accessory: UITableViewCellAccessoryDisclosureIndicator],
                       [TTTableTextItem itemWithText: NSLocalizedString(@"Deleted", nil)
                                           accessory: UITableViewCellAccessoryDisclosureIndicator],
                       nil];
}

- (void) didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UCBREW_CALL_METHOD;
    NSString* box_id = nil;
    switch (indexPath.row) {
        case 0:
            box_id = @"inbox";
            break;
        case 1:
            box_id = @"outbox";
            break;
        case 2:
            box_id = @"deleted";
            break;
        default:
            break;
    }
    if (UcBrew_IsNotEmptyString(box_id)) {
        USMailboxVC* vc = [USMailboxVC viewControllerWithBoxId: box_id];
        [self.navigationController pushViewController: vc animated: YES];
    }
}
@end