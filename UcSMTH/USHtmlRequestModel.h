//
//  USHtmlRequestModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLRequestModel.h"
#import "USModelAuthChecker.h"

@interface USHtmlRequestModel : USURLRequestModel <USModelAuthChecker>
- (id<USModelAuthChecker>) checker;
@end
