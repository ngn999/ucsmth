//
// Created by kerberos on 12-10-8.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USImagePanel.h"


@implementation USImagePanel
@synthesize delegate = _delegate;

- (void) dealloc
{
    [super dealloc];
}

- (NSInteger) tableView:(UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    return 1;
}

- (UITableViewCell*) tableView: (UITableView*) tableView cellForRowAtIndexPath: (NSIndexPath*) indexPath
{
    static NSString* identifier = @"UAModelPanelCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier] autorelease];
    }

    return cell;
}
@end