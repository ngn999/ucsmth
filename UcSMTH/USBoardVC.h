//
//  USBoardVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableViewController.h"
#import "USPageVC.h"
@interface USBoardVC : USTableViewController <USPageVCDelegate>
+ (id) viewControllerWithBoardId: (NSString*) boardId;
@end
