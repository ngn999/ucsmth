//
//  USForumDS.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "FMDatabase.h"
#import "FMResultSet.h"
#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USForumDS.h"

////////////////////////////////////////////////////////////////////////////////
@interface USForumDS ()
@property (nonatomic, retain) FMDatabase* sqlite;
@property (nonatomic, retain) NSMutableArray* sectionIndexArray;
@end

////////////////////////////////////////////////////////////////////////////////
@implementation USForumDS
@synthesize sqlite;
@synthesize sectionIndexArray;

+ (id) dataSource
{
    USForumDS* ds = [[USForumDS alloc] init];
    
    return [ds autorelease];
}

- (id) init
{
    self = [super init];
    if (self) {
        [self loadFromDatabase];
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(sectionIndexArray);
    UCBREW_RELEASE(sqlite);
    [super dealloc];
}

- (id<TTModel>) model
{
    return self;
}

- (NSString*) titleForEmpty
{
    return NSLocalizedString (@"Can't load boards.", nil);
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.sectionIndexArray;
}

- (void) loadFromDatabase
{
    NSMutableArray* section_items = [NSMutableArray arrayWithCapacity:16];
    NSMutableArray* row_items = [NSMutableArray arrayWithCapacity: 16];
    self.sectionIndexArray = [NSMutableArray arrayWithCapacity: 16];
    
    self.sqlite = [FMDatabase databaseWithPath: [[UcSMTHEngine sharedEngine] cacheFilePath]];
    if ([sqlite open]) {
        FMResultSet* rs = [self.sqlite executeQuery: @"SELECT name, id from section;"];
        while ([rs next]) {
            NSMutableArray* board_array = [NSMutableArray arrayWithCapacity: 64];
            NSString* name = [rs stringForColumn: @"name"];
            NSString* bid = [rs stringForColumn: @"id"];
            FMResultSet* crs = [self.sqlite executeQuery: @"SELECT name, id from board where section = ?" , bid];
            while ([crs next]) {
                NSString* board_name = [crs stringForColumn: @"name"];
                NSString* board_id = [crs stringForColumn: @"id"];
                UTTableLongSubtitleItem * item = nil;
                item = [UTTableLongSubtitleItem itemWithText: board_name
                                                    subtitle: [NSString stringWithFormat: @"[ %@ ]", board_id]
                                                   accessory: UITableViewCellAccessoryDisclosureIndicator];
                item.userInfo = [NSString stringWithString: board_id];
                [board_array addObject: item];
            }
            
            if (!! board_array.count) {
                [row_items addObject: board_array];
                [section_items addObject: [NSString stringWithString: name]];
                [self.sectionIndexArray addObject: name];
            }
        }
    }
    
    self.items = row_items;
    self.sections = section_items;
}
@end