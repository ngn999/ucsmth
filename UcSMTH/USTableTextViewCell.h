//
//  USTableTextViewCell.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "MwfTableItem.h"

@interface USTableTextViewCell : MwfTableItemCell
@property (nonatomic, retain) UITextView* textView;
@end
