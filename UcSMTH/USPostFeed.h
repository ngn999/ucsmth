//
//  USPostFeed.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USPostFeed : NSObject <NSCopying>
@property (nonatomic, copy) NSString* postId;
@property (nonatomic, copy) NSString* postTitle;
@property (nonatomic, copy) NSString* postDate;
@property (nonatomic, copy) NSString* postAuthorId;
@property (nonatomic, copy) NSString* postBoardId;
@property (nonatomic, assign) BOOL isTop;
@end