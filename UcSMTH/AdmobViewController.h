//
//  AdmobControllerViewController.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 13-5-8.
//  Copyright (c) 2013年 UcBrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerView.h"
#import "GADBannerViewDelegate.h"

@interface AdmobViewController : UIViewController <GADBannerViewDelegate>
@property (nonatomic, retain) GADBannerView* admobView;
@end
