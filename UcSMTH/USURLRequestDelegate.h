//
//  USURLRequestDelegate.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTURLRequestDelegate.h"

@protocol USURLRequestDelegate <NSObject, TTURLRequestDelegate>

@end
