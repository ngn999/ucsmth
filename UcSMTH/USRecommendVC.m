//
//  USRecommendVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USPostFeed.h"
#import "USArticleWebVC.h"
#import "USRecommendDS.h"
#import "USRecommendVC.h"

@interface USRecommendVC ()

@end

@implementation USRecommendVC

+ (id) viewController
{
    USRecommendVC* vc = [[USRecommendVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    return NSLocalizedString(@"Recommend", nil);
}
- (void) createModel
{
    self.dataSource = [USRecommendDS dataSource];
}

- (id) createDelegate
{
    TTTableViewDragRefreshDelegate* d = nil;
    d = [[[TTTableViewDragRefreshDelegate alloc] initWithController: self]
         autorelease];
    return d;
}

- (void) didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    TTTableItem* item = UCBREW_DYNAMIC_CAST(object, TTTableItem);
    if (item) {
        USPostFeed* feed = UCBREW_DYNAMIC_CAST(item.userInfo, USPostFeed);
        UCBREW_LOG_OBJECT(feed.postId);
        UCBREW_LOG_OBJECT(feed.postBoardId);
        
        if (feed.postBoardId && feed.postId) {
            USArticleWebVC* vc = [USArticleWebVC viewControllerWithBoardId: feed.postBoardId articleId: feed.postId];
            [self.navigationController pushViewController: vc animated: YES];
        }
    }
}
@end
