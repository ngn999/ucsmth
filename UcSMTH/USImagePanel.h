//
// Created by kerberos on 12-10-8.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "UATitledModalPanel.h"

@class USImagePanel;

@protocol USImagePanelDelegate <NSObject>
@optional
- (void) panelDidFinish: (USImagePanel*) panel;
@end

@interface USImagePanel : UATitledModalPanel <UITableViewDataSource>
@property (nonatomic, assign) id<USImagePanelDelegate> delegate;
@end