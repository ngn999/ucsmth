//
//  UTTableRightTextItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UTTableRightTextItemCell.h"
#import "UTTableRightTextItem.h"

@implementation UTTableRightTextItem
+ (id) itemWithText:(NSString *)text
{
    UTTableRightTextItem* item = [[UTTableRightTextItem alloc] init];
    item.text = text;
    return [item autorelease];
}

+ (id) itemWithText:(NSString *)text accessory:(UITableViewCellAccessoryType)accessoryType
{
    UTTableRightTextItem* item = [[UTTableRightTextItem alloc] init];
    item.text = text;
    item.accessoryType = accessoryType;
    return [item autorelease];
}

+ (id) itemWithText:(NSString *)text delegate:(id)delegate selector:(SEL)selector
{
    UTTableRightTextItem* item = [[UTTableRightTextItem alloc] init];
    item.text = text;
    item.selector = selector;
    return [item autorelease];
}

- (Class) cellClass
{
    return [UTTableRightTextItemCell class];
}

@end
