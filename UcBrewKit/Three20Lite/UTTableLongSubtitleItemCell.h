//
//  UTTableLongSubtitleItemCell.h
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-15.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "Three20Lite.h"

@interface UTTableLongSubtitleItemCell : TTTableLinkedItemCell
@property (nonatomic, readonly, retain) UILabel* subtitleLabel;
@end
