//
//  UTTableMoreButtonCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UTTableMoreButtonCell.h"

static const CGFloat kMoreButtonMargin = 40.0f;

@implementation UTTableMoreButtonCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)setObject:(id)object {
    if (_item != object) {
        [super setObject:object];
        
        UTTableMoreButton* item = object;
        self.animating = item.isLoading;
        
        self.textLabel.textColor = TTSTYLEVAR(textColor);
        self.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        self.textLabel.numberOfLines = 1;
        self.selectionStyle = TTSTYLEVAR(tableSelectionStyle);
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    /*
     * calc button width
     */
    NSString* labelText = self.textLabel.text;
    CGFloat maxLablelWidth = self.contentView.width - (_activityIndicatorView.width - kTableCellSmallMargin) * 2;
    CGSize constrainSize = CGSizeMake(maxLablelWidth, TTSTYLEVAR(tableFont).ttLineHeight);
    CGSize trueSize = [labelText sizeWithFont: TTSTYLEVAR(tableFont) constrainedToSize: constrainSize lineBreakMode: UILineBreakModeTailTruncation];
    CGFloat left = floorf((self.contentView.width - trueSize.width) / 2);
    self.textLabel.frame = CGRectMake(left, self.textLabel.top, trueSize.width, TTSTYLEVAR(tableFont).ttLineHeight);
    self.textLabel.top = floor ((self.contentView.height - self.textLabel.height) / 2);
    /*
     * _activityIndicatorView
     */
    _activityIndicatorView.right = self.textLabel.left - kTableCellSmallMargin;
    /*
     * disable detailTextLabel
     */
    self.detailTextLabel.frame = CGRectZero;
}
@end
